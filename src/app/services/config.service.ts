import { ApiService } from './api.service';
import { Injectable } from "@angular/core";
import { Router, CanActivate, CanLoad } from "@angular/router";
import {
    HttpClient
} from "@angular/common/http";
@Injectable()
export class ConfigService {
    config: any;
    constructor(private _http: HttpClient) {
    }
    getConfig() {
        return this._http.get('assets/config.json').toPromise();
    }
    getReportLevel() {
        return [{ name: 'Organization', value: 'ORG' }, { name: 'Manager', value: 'MGR' }];
    }
    getMonths() {
        return [
            { name: 'January', value: 'JAN' },
            { name: 'February', value: 'FEB' },
            { name: 'March', value: 'MAR' },
            { name: 'April', value: 'APR' },
            { name: 'May', value: 'MAY' },
            { name: 'June', value: 'JUN' },
            { name: 'July', value: 'JUL' },
            { name: 'August', value: 'AUG' },
            { name: 'September', value: 'SEP' },
            { name: 'October', value: 'OCT' },
            { name: 'November', value: 'NOV' },
            { name: 'December', value: 'DEC' }
        ];
    }
    getYears() {
        const date = new Date();
        let years = [];
        for (let i = 0; i < 5; i++) {
            years.push({ name: date.getFullYear() - i, value: date.getFullYear() - i });
        }
        return years;
    }
}
