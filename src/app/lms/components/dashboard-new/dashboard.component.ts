import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ApiService } from '../../../services/api.service';
import { ConfigService } from '../../../services/config.service';

@Component({
  selector: 'lms-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class LmsDashboardComponent implements OnInit {
  module: string = "Learning Management Dashboard";
  dashboardFilters: any = {};
  lstReportLevel: any[] = [];
  lstMoths: any[] = [];
  lstYears: any[] = [];
  constructor(private _apiService: ApiService, private _configService: ConfigService) { }

  public ngOnInit() {
    this.lstReportLevel = this._configService.getReportLevel();
    this.lstMoths = this._configService.getMonths();
    this.lstYears = this._configService.getYears();
    this.dashboardFilters.reportLevel = this.lstReportLevel[0].value;
    this.dashboardFilters.quarterStartMonth = this.lstMoths[0].value;
    this.dashboardFilters.reportYear = this.lstYears[0].value;
    this.getLMSdashboard();
    const barCourseData = [{
      name: 'Incomplete 1',
      data: [49.9, 71.5, 106.4, 129.2, 144.0]

    }, {
      name: 'Incomplete 2',
      data: [83.6, 78.8, 98.5, 93.4, 106.0]

    }, {
      name: 'Incomplete 3',
      data: [48.9, 38.8, 39.3, 41.4, 47.0]

    }];
    const barPendingData = [{
      name: 'Course 1',
      data: [49.9, 71.5, 106.4, 129.2, 144.0]

    }, {
      name: 'Course 2',
      data: [83.6, 78.8, 98.5, 93.4, 106.0]

    }, {
      name: 'Course 3',
      data: [48.9, 38.8, 39.3, 41.4, 47.0]

    }];
    const barCourseCategories = [
      'Not Started',
      'In Progress',
      'Completed',
      'Failed',
      'Passed'
    ];
    const barPendingCategories = [
      'Manager 1',
      'Manager 2',
      'Manager 3',
      'Manager 4'
    ]
    // const bieIncompleteData = [{
    //   name: 'Year',
    //   colorByPoint: true,
    //   data: [{
    //     name: '1st Qtr',
    //     y: 61.41,
    //     sliced: true,
    //     selected: true
    //   }, {
    //     name: '2nd Qtr',
    //     y: 11.84
    //   }, {
    //     name: '3rd Qtr',
    //     y: 10.85
    //   }, {
    //     name: '4th Qtr',
    //     y: 4.67
    //   }]
    // }];
    // const bieCompleteData = [{
    //   name: 'Year',
    //   colorByPoint: true,
    //   data: [{
    //     name: 'E-Learning',
    //     y: 61.41,
    //     sliced: true,
    //     selected: true
    //   }, {
    //     name: 'Class Room Based',
    //     y: 11.84
    //   }, {
    //     name: 'Internal',
    //     y: 10.85
    //   }, {
    //     name: 'External',
    //     y: 4.67
    //   }]
    // }];
    const complianceData = [{
      name: 'Passed',
      data: [4.5, 2.5, 3.5, 4.8]
    }, {
      name: 'Failed',
      data: [2.5, 4.5, 1.7, 3]
    }];
    const complianceCategories=['Course 1','Course 2','Course 3','Course 4'];
    const companyComplianceCategories=['Company A','Company B','Company C','Company D'];
    // this.loadBieChart('incompleteCourse-container', 'Incomplete Courses', bieIncompleteData);
    // this.loadBieChart('completeLearn-container', 'Complete Learning by Type', bieCompleteData);
    this.loadBarChart('courseStatus-container', 'Course Status', barCourseData, barCourseCategories);
    this.loadLineChart('complianceRates-container', 'Compliance Rates', complianceData, complianceCategories);
    this.loadBarChart('pendingApproval-container', 'Pending Approval(s)', barPendingData, barPendingCategories);
    this.loadLineChart('companyComplianceRates-container', 'Compay Compliance Rates', complianceData, companyComplianceCategories);
  }
  private loadBarChart(id: string, chartTitle: string, data: any[], listCategories: string[]) {
    Highcharts.chart(id, {
      chart: {
        type: 'column'
      },
      title: {
        text: chartTitle
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: listCategories,
        crosshair: true
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: data
    });
  }
  private loadBieChart(id: string, chartTitle: string, chartSeries: any[]) {
    Highcharts.chart(id, {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: chartTitle
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: chartSeries
    });
  }
  private loadLineChart(id: string, chartTitle: string, chartData: any[], chartCategories: string[]) {
    Highcharts.chart(id, {
      title: {
        text: chartTitle
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      xAxis: {
        categories: chartCategories
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: false
          },
          enableMouseTracking: false
        }
      },
      series: chartData,
      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  }
  getLMSdashboard() {
    this._apiService.get('/dashboard/lms/get-incomplete-courses-qtr-results/reportLevel/'+this.dashboardFilters.reportLevel+'/quarterStartMonth/'+this.dashboardFilters.quarterStartMonth+'/reportYear/'+this.dashboardFilters.reportYear+'').subscribe(
      (data: any) => {
        const bieIncompleteData = [{
          name: 'Year',
          colorByPoint: true,
          data: [{
            name: '1st Qtr',
            y: data.firstQuarter,
            sliced: true,
            selected: true
          }, {
            name: '2nd Qtr',
            y: data.secondQuarter
          }, {
            name: '3rd Qtr',
            y: data.thirdQuarter
          }, {
            name: '4th Qtr',
            y: data.fourthQuarter
          }]
        }];
        this.loadBieChart('incompleteCourse-container', 'Incomplete Courses', bieIncompleteData);
      },
      (error) => {
        console.log(error);
      }
    )
    this._apiService.get('/dashboard/lms/get-completed-learning-type-distribution/reportLevel/'+this.dashboardFilters.reportLevel+'').subscribe(
      (data: any) => {
        const propName = this.dashboardFilters.reportLevel === 'ORG' ? 'completedOrgLevel' : 'completedMgrLevel';
        let chartData = [];
        data[propName].forEach((item: any, index: number) => {
          if(index===0){
            chartData.push({
              name: item.learningTypeDescription,
              y: item.count,
              sliced: true,
              selected: true
            });
          }else{
          chartData.push({
            name: item.learningTypeDescription,
            y: item.count
          });
        }
        });
        const bieCompleteData = [{
          name: 'Year',
          colorByPoint: true,
          data: chartData
        }];
        this.loadBieChart('completeLearn-container', 'Complete Learning by Type', bieCompleteData);
      },
      (error) => {
        console.log(error);
      }
    )
  }
}
